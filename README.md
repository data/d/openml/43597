# OpenML dataset: 150K-Lyrics-Labeled-with-Spotify-Valence

https://www.openml.org/d/43597

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Based on 250K lyrics database. Created to perform Supervised NLP sentiment analysis task using Spotify valence audio feature, a measure of the positiveness of the song.
Content
Preparation of the dataset is explained in this notebook.  
Acknowledgements
Thank you Nikita Detkov and Ilya for making the great 250K Lyrics1.csv file that I used for this data set. 
Thank you Madeline Zhang for the commented Spotify access example code and Spotify for the detailed Developers Spotify API.
Inspiration
Analysis of lyrics in relation to other song audio features.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43597) of an [OpenML dataset](https://www.openml.org/d/43597). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43597/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43597/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43597/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

